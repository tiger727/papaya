package im.papaya.model.entity;

public final class VideoPlayListItem {

    private int avatarRes;
    private String videoUrl;
    private String userName;
    private String content;
    private String coverUrl;
    private String avatarUrl;
    private int videoWidth;
    private int videoHeight;

    public VideoPlayListItem(int avatarRes, String videoUrl, String userName, String content, String coverUrl, int videoWidth, int videoHeight) {
        this.avatarRes = avatarRes;
        this.videoUrl = videoUrl;
        this.userName = userName;
        this.content = content;
        this.coverUrl = coverUrl;
        this.videoWidth = videoWidth;
        this.videoHeight = videoHeight;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getUserName() {
        return userName;
    }

    public String getContent() {
        return content;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public int getVideoWidth() {
        return videoWidth;
    }

    public int getVideoHeight() {
        return videoHeight;
    }

    public int getAvatarRes() {
        return avatarRes;
    }
}
