package im.papaya.view.editor;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

import im.papaya.android.R;
import im.papaya.ui.activity.PlaybackActivity;
import im.papaya.view.editor.playview.TuSdkMovieScrollContent;
import im.papaya.view.editor.playview.TuSdkRangeSelectionBar;
import im.papaya.view.editor.ruler.RulerView;

/**
 * droid-sdk-video
 *
 * @author MirsFang
 * @Date 2018/9/21 15:21
 * @Copright (c) 2018 tusdk.com. All rights reserved.
 * <p>
 * 编辑里选取裁剪的View
 */
public class EditorCutView {
    private static final String TAG = EditorCutView.class.toString();
    //时间选择
    private TuSdkMovieScrollContent mRangeView;
    //刻度
//    private RulerView mRulerView;
    private TextView mTimeRangView;
    //最小裁剪时间
    private long mMinCutTimeUs =  3 * 1000000;
    private boolean isEnable = true;
    private PlaybackActivity mActivity;


    public EditorCutView(PlaybackActivity activity) {
        this.mActivity = activity;
        init();
    }



    /** 获取布局Id */
    protected int getLayoutId() {
        return R.layout.editor_cut_view;
    }

    /** 初始化View */
    private void init() {
        mRangeView = mActivity.findViewById(R.id.lsq_range_line);
        mRangeView.setType(1);
        mRangeView.setShowSelectBar(true);
        mRangeView.setNeedShowCursor(true);
//        mRulerView = mActivity.findViewById(R.id.lsq_rule_view);
        mTimeRangView = mActivity.findViewById(R.id.lsq_range_time);
    }

    private int mTwoBarsDistance = 0;
    public void setTwoBarsMinDistance(int twoBarsDistance){
        this.mTwoBarsDistance = twoBarsDistance;
//        mRangeView.setTwoBarsMinDistance(twoBarsDistance);
    }

    public void setMinCutTimeUs(float timeUs){
        mRangeView.setMinWidth(timeUs);
    }

    /**
     * 设置时间区间
     * @param times
     */
    public void setRangTime(float times){
        String rangeTime = String.format("%s %.1f %s",mActivity.getResources().getString(R.string.lsq_movie_cut_selecttime),times,"s");
        mTimeRangView.setText(rangeTime);
    }

    /**
     * 设置封面图
     * @param coverList 封面图列表
     */
    public void setCoverList(List<Bitmap> coverList){
        if(coverList == null){
            Log.e(TAG, " bitmap list of cover is null !!!");
            return;
        }
//        mRangeView.setBitmapList(coverList);
//        mRangeView.setMinSelectTimeUs(mMinCutTimeUs);
    }

    /**
     * 设置视频的总时长
     * @param totalTimeUs
     */
    public void setTotalTime(long totalTimeUs){
        if(totalTimeUs <= 0)
        {
            Log.e(TAG, " video time length mast > 0  !!!");
            return;
        }
//        mRulerView.setMaxValueAndPaintColor(totalTimeUs, mActivity.getResources().getColor(R.color.lsq_color_white));
    }

    /**
     * 设置选择区间回调
     * @param onSelectTimeChangeListener
     */
    public void setOnSelectCeoverTimeListener(TuSdkRangeSelectionBar.OnSelectRangeChangedListener onSelectTimeChangeListener){
        if(onSelectTimeChangeListener == null)
        {
            Log.e(TAG,"setSelectCoverTimeListener is null !!!");
            return;
        }
        mRangeView.setSelectRangeChangedListener(onSelectTimeChangeListener);
    }

    /**
     * 播放指针 位置改变监听
     * @param progressChangeListener
     */
    public void setOnPlayPointerChangeListener(TuSdkMovieScrollContent.OnPlayProgressChangeListener progressChangeListener){
        if(progressChangeListener == null){
            Log.e(TAG,"setSelectCoverTimeListener is null !!!");
            return;
        }
        mRangeView.setProgressChangeListener(progressChangeListener);
    }


    /**
     * 设置播放进度
     * @param percent 播放进度的百分比
     */
    public void setVideoPlayPercent(float percent){
        if(mRangeView == null) return;;
        if(percent < 0){
            Log.e(TAG,"setSelectCoverTimeListener is null !!!");
            return;
        }
        mRangeView.setPercent(percent);
    }

    public TuSdkMovieScrollContent getLineView(){
        return mRangeView;
    }

    public void addBitmap(Bitmap bitmap) {
        mRangeView.addBitmap(bitmap);
    }

    /** 设置是否启用 **/
    public void setEnable(boolean isEnable){
        this.isEnable = isEnable;
        mRangeView.setEnable(isEnable);
    }


}
