package im.papaya.ui.fragment;

import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.movieous.media.mvp.model.entity.UFilter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import im.papaya.Constants;
import im.papaya.android.R;
import im.papaya.mvp.contract.IMagicStickerSelectedListener;
import im.papaya.mvp.model.entity.FilterVendor;
import io.inchtime.recyclerkit.RecyclerAdapter;
import io.inchtime.recyclerkit.RecyclerKit;
import kotlin.Unit;

/**
 * 贴纸选择页面
 */
public class StickerFilterFragment extends BottomPanelFragment {

    @BindView(R.id.sticker_filter_recycler)
    RecyclerView mStickerRecyclerView;

    @BindView(R.id.iv_close)
    ImageView iv_close;

    private RecyclerAdapter mStickerAdapter;
    private List<UFilter> items = new ArrayList<>();
    private ImageView mCurrentFilterIcon;
    private IMagicStickerSelectedListener magicStickerSelectedListener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sticker_filter;
    }

    @Override
    public void initView() {
        setupRecyclerView();
    }

    public void setFilterItems(List<UFilter> items) {
        this.items = items;
    }

    private void setupRecyclerView() {
        mStickerAdapter = RecyclerKit.INSTANCE.adapter(mActivity, Constants.SPAN_COUNT)
                .recyclerView(mStickerRecyclerView)
                .withGridLayout(GridLayoutManager.VERTICAL, false)
                .modelViewBind((index, viewModel, viewHolder) -> {
                    UFilter item = (UFilter) viewModel.getValue();
                    ImageView imageView = viewHolder.findView(R.id.icon);
                    if (item.getVendor() == FilterVendor.FACEUNITY) {
                        imageView.setImageResource(item.getResId());
                    } else {
                        imageView.setImageBitmap(item.getIcon());
                    }
                    return Unit.INSTANCE;
                })
                .modelViewClick((pIndex, pViewModel, view) -> {
                    clearIconState();
                    ImageView imageView = view.findViewById(R.id.icon);
                    imageView.setBackgroundResource(R.drawable.control_filter_select);
                    mCurrentFilterIcon = imageView;

                    if(magicStickerSelectedListener != null){
                        magicStickerSelectedListener.onMagicStickerSelected((UFilter) pViewModel.getValue());
                    }
                    return Unit.INSTANCE;
                })
                .emptyViewBind((emptyViewHolder -> Unit.INSTANCE))
                .build();
        mStickerAdapter.setEmptyView(R.layout.recyclerkit_view_empty);
        loadStickerModels();
    }

    private void loadStickerModels() {

        List<RecyclerAdapter.ViewModel> mViewModels = new ArrayList<>();

        for (UFilter item : items) {
            mViewModels.add(new RecyclerAdapter.ViewModel(R.layout.item_filter_view, 1, RecyclerAdapter.ModelType.LEADING, item, false));
        }

        clearIconState();
        mStickerAdapter.setModels(mViewModels);
    }

    private void clearIconState() {
        if (mCurrentFilterIcon != null) {
            mCurrentFilterIcon.setBackgroundResource(0);
        }
    }

    @OnClick(R.id.iv_close)

    public void OnCloseClicked(){
        if(onCloseClickListener != null){
            onCloseClickListener.onCloseClick();
        }
    }

    public void setMagicStickerSelectedListener(IMagicStickerSelectedListener magicStickerSelectedListener) {
        this.magicStickerSelectedListener = magicStickerSelectedListener;
    }
}
