package im.papaya.mvp.contract;

import im.papaya.mvp.model.entity.BeautyParamEnum;

public interface IFaceBeautyParamChangedListener {
    void onBeautyParamChanged(BeautyParamEnum beautyType, float value);
}
