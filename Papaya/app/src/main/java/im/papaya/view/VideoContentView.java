package im.papaya.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

/**
 * droid-sdk-video
 *
 * @author MirsFang
 * @Date 2018/9/26 14:46
 * @Copright (c) 2018 tusdk.com. All rights reserved.
 * <p>
 * 视频编辑中播放器所在的Content
 */
public class VideoContentView extends RelativeLayout {


    public VideoContentView(Context context) {
        super(context);
    }

    public VideoContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 设置宽度
     *
     * @param width
     */
    public void setWidth(int width) {
        ViewGroup.LayoutParams params = getLayoutParams();
        if(params == null) return;
        params.width = width;
        setLayoutParams(params);
    }

    /**
     * 设置当前控件高度
     *
     * @param height
     */
    public void setHeight(int height) {
        ViewGroup.LayoutParams params = getLayoutParams();
        if(params == null) return;
        params.height = height;
        setLayoutParams(params);
    }
}
