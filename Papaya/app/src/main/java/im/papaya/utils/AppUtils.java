package im.papaya.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;

import org.jetbrains.annotations.NotNull;

public class AppUtils {
    public static int getVerName(@NotNull Context context) {
        int verCode = -1;

        try {
            String packageName = context.getPackageName();
            verCode = context.getPackageManager().getPackageInfo(packageName, 0).versionCode;
        } catch (PackageManager.NameNotFoundException var4) {
            var4.printStackTrace();
        }

        return verCode;
    }

    @NotNull
    public static Intent getMediaIntent(boolean isVideo) {
        Intent intent = new Intent();
        String intentType = isVideo ? "video/*" : "audio/*";
        intent.setAction("android.intent.action.OPEN_DOCUMENT");
        intent.addCategory("android.intent.category.OPENABLE");
        intent.setType(intentType);

        return intent;
    }
}
