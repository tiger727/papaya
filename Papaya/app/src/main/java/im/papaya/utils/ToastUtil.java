package im.papaya.utils;

import android.content.Context;
import android.widget.Toast;

public  class ToastUtil {

    public static final ToastUtil INSTANCE;

    private ToastUtil() {
    }

    static {
        INSTANCE = new ToastUtil();
    }


    public void showToast( Context context, String content){
        Toast toast = Toast.makeText(context, content, Toast.LENGTH_SHORT);
        toast.show();
    }
}
