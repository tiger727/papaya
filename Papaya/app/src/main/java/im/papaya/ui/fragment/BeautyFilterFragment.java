package im.papaya.ui.fragment;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.movieous.media.mvp.model.entity.UFilter;

import butterknife.OnClick;
import im.papaya.mvp.contract.IFaceBeautyParamChangedListener;
import im.papaya.mvp.contract.IFilterSelectedListener;
import im.papaya.mvp.model.entity.BeautyParamEnum;
import im.papaya.mvp.model.entity.TabEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import im.papaya.android.R;
import im.papaya.ui.adapter.FilterAdapter;

/**
 * 美颜设置页面
 */
public class BeautyFilterFragment extends BottomPanelFragment implements SeekBar.OnSeekBarChangeListener, OnTabSelectListener {
    public static final boolean SDK_BEAUTY = false;
    public static final int SHOW_TYPE_BEAUTY = 1;
    public static final int SHOW_TYPE_FILTER = 0;
    public static float sBlurLevel = 0.7f;//磨皮
    public static float sEyeEnlarging = 0.4f;//大眼
    public static float sCheekThinning = 0.4f;//瘦脸

    private int mTitleButtonIndex = SHOW_TYPE_FILTER;

    @BindView(R.id.tv_title)
    TextView mTabTitle;
    @BindView((R.id.layout_content))
    LinearLayout mLayoutContent;
    @BindView(R.id.tab_title)
    CommonTabLayout mTabLayout;
    @BindView(R.id.iv_close)
    ImageView iv_close;

    private String[] mTabTitles;
    private LinearLayout mLayoutFilter;
    private LinearLayout mLayoutBeauty;
    private FilterAdapter mFilterAdapter;
    private List<UFilter> uFilters;

    private IFilterSelectedListener filterSelectedListener;
    private IFaceBeautyParamChangedListener faceBeautyParamChangedListener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_beauty_filter;
    }

    @Override
    public void initView() {
        mTabTitles = new String[]{getString(R.string.btn_preview_filter), getString(R.string.btn_preview_beauty)};
        ArrayList<CustomTabEntity> tabEntities = new ArrayList<>();
        for (int i = 0; i < mTabTitles.length; i++) {
            tabEntities.add(new TabEntity(mTabTitles[i], 0, 0));
        }
        mTabLayout.setTabData(tabEntities);
        mTabLayout.setOnTabSelectListener(this);
        showContentLayout(mTitleButtonIndex);
    }

    @Override
    public void onTabSelect(int position) {
        showContentLayout(position);
    }

    @Override
    public void onTabReselect(int position) {
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int viewId = seekBar.getId();
        BeautyParamEnum beautyType = (viewId == R.id.value_progress_blur)
                ? BeautyParamEnum.FACE_BLUR : (viewId == R.id.value_progress_eye)
                ? BeautyParamEnum.EYE_ENLARGE : BeautyParamEnum.CHEEK_THINNING;
        if(faceBeautyParamChangedListener != null)
            faceBeautyParamChangedListener.onBeautyParamChanged( beautyType, 1.0f * progress / seekBar.getMax());
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @OnClick(R.id.iv_close)

    public void OnCloseClicked(){
        if(onCloseClickListener != null){
            onCloseClickListener.onCloseClick();
        }
    }

    private void showContentLayout(int index) {
        mTitleButtonIndex = index;
        mTabTitle.setText(mTabTitles[index]);
        if (index == SHOW_TYPE_BEAUTY) {
            showFaceBeautyLayout();
        } else if (index == SHOW_TYPE_FILTER) {
            showBeautyFilterLayout();
        }
    }

    /**
     * 显示美颜视图布局
     */
    private void showFaceBeautyLayout() {
        if (mLayoutBeauty == null) {
            mLayoutBeauty = (LinearLayout) mInflater.inflate(R.layout.view_beauty, null);
            SeekBar blurSeekbar = mLayoutBeauty.findViewById(R.id.value_progress_blur);
            blurSeekbar.setOnSeekBarChangeListener(this);
            blurSeekbar.setProgress((int) (sBlurLevel * 100));
            SeekBar faceSeekbar = mLayoutBeauty.findViewById(R.id.value_progress_face);
            faceSeekbar.setOnSeekBarChangeListener(this);
            faceSeekbar.setProgress((int) (sCheekThinning * 100));
            SeekBar eyeSeekbar = mLayoutBeauty.findViewById(R.id.value_progress_eye);
            eyeSeekbar.setOnSeekBarChangeListener(this);
            eyeSeekbar.setProgress((int) (sEyeEnlarging * 100));
        }
        mLayoutContent.removeAllViews();
        mLayoutContent.addView(mLayoutBeauty);
    }

    /**
     * 显示滤镜布局
     */
    private void showBeautyFilterLayout() {
        if (mLayoutFilter == null) {
            mLayoutFilter = (LinearLayout) mInflater.inflate(R.layout.view_filter, null);
            RecyclerView recyclerView = mLayoutFilter.findViewById(R.id.beauty_filter_recyclerView);
            LinearLayoutManager mFilterLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(mFilterLayoutManager);
            mFilterAdapter = new FilterAdapter(mActivity, this.filterSelectedListener, uFilters);
            recyclerView.setAdapter(mFilterAdapter);
        }
        mLayoutContent.removeAllViews();
        mLayoutContent.addView(mLayoutFilter);
    }

    public void setFilterSelectedListener(IFilterSelectedListener filterSelectedListener) {
        this.filterSelectedListener = filterSelectedListener;
    }

    public void setFaceBeautyParamChangedListener(IFaceBeautyParamChangedListener faceBeautyParamChangedListener) {
        this.faceBeautyParamChangedListener = faceBeautyParamChangedListener;
    }

    public void setuFilters(List<UFilter> uFilters) {
        this.uFilters = uFilters;
    }
}