package im.papaya.ui.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.List;

import im.papaya.android.R;
import im.papaya.mvp.contract.OnItemClickListener;
import im.papaya.mvp.model.entity.VideoFileItem;

public class VideoSelectionListAdapter extends RecyclerView.Adapter<VideoSelectionListAdapter.VideoSelectableItemRecyclerHolder> {

    private Context context;
    private List<VideoFileItem> videoListItems;
    private int itemSize;
    private OnItemClickListener<VideoFileItem> onItemClickListener;

    public VideoSelectionListAdapter(Context context, List<VideoFileItem> videoListItems, int itemSize, OnItemClickListener<VideoFileItem> onItemClickListener) {
        this.context = context;
        this.videoListItems = videoListItems;
        this.itemSize = itemSize;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public VideoSelectableItemRecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VideoSelectionListAdapter.VideoSelectableItemRecyclerHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.view_video_select_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull VideoSelectableItemRecyclerHolder holder, int position) {
        holder.bindMedia(videoListItems.get(position));
    }

    @Override
    public int getItemCount() {
        return this.videoListItems.size();
    }

    public class VideoSelectableItemRecyclerHolder extends RecyclerView.ViewHolder {

        private ImageView mThumbnail;
        private TextView mVideoDuration;
        private VideoFileItem videoFileItem;

        public VideoSelectableItemRecyclerHolder(@NonNull View itemView) {
            super(itemView);
            mThumbnail = itemView.findViewById(R.id.media_thumbnail);
            mVideoDuration = itemView.findViewById(R.id.video_duration);
        }

        public void bindMedia(VideoFileItem item) {
            videoFileItem = item;
            Glide.with(context).load(Uri.fromFile(new File(videoFileItem.getPath()))).override(itemSize, itemSize)
                    .centerCrop().into(mThumbnail);
            mVideoDuration.setText(videoFileItem.getStringDuration());
            mThumbnail.setOnClickListener(view -> onItemClickListener.onItemClicked(videoFileItem));
        }
    }
}
