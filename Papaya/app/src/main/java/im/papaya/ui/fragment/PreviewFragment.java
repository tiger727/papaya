package im.papaya.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import butterknife.BindView;
import im.papaya.android.R;
import im.papaya.mvp.model.entity.MediaParam;
import im.papaya.ui.base.BaseFragment;
import im.papaya.utils.SharePrefUtils;
import im.papaya.utils.ToastUtil;
import im.papaya.vendor.faceunity.FuSDKManager;
import im.papaya.view.SaveProgressDialog;
import video.movieous.engine.UAVOptions;
import video.movieous.engine.UVideoFrameListener;
import video.movieous.engine.UVideoSaveListener;
import video.movieous.engine.view.UTextureView;


public class PreviewFragment extends BaseFragment
        implements UVideoFrameListener,
        UVideoSaveListener {
    private static final String TAG = "PreviewFragment";

    protected Activity mActivity;
    @BindView(R.id.preview)
    protected UTextureView mPreview;
    protected SaveProgressDialog mProcessingDialog;
    protected FuSDKManager mFilterSdkManager;
    protected UAVOptions mAVOptions; // 可以自定义 SDK 参数

    protected void initProcessingDialog() {
        mProcessingDialog = new SaveProgressDialog(mActivity);
    }

    protected void showProcessingDialog() {
        if (mProcessingDialog == null) {
            initProcessingDialog();
        }
        mProcessingDialog.show();
    }

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public void initView() {
    }

    @Override
    public void lazyLoad() {
        MediaParam mediaParam = SharePrefUtils.getParam(mActivity);
        initAVOptions(mediaParam);
    }

    // 自定义音视频输出参数，如果不自定义的话，会采用默认参数，参见 {@link UConstants}
    private void initAVOptions(MediaParam mediaParam) {
        mAVOptions = new UAVOptions()
                .setInteger(UAVOptions.KEY_VIDEO_WIDTH, mediaParam.width)
                .setInteger(UAVOptions.KEY_VIDEO_HEIGHT, mediaParam.height)
                .setInteger(UAVOptions.KEY_VIDEO_BITRATE, mediaParam.videoBitrate * 1000)
                .setInteger(UAVOptions.KEY_VIDEO_FPS, mediaParam.videoFrameRate);
    }

    // UVideoFrameListener
    @Override
    public void onSurfaceCreated() {
        mFilterSdkManager.onSurfaceCreated();
    }

    @Override
    public void onSurfaceChanged(int width, int height) {
        Log.i(TAG, "onSurfaceChanged, width = " + width + ", height = " + height);
        mFilterSdkManager.onSurfaceChanged(width, height);
    }

    @Override
    public void onSurfaceDestroyed() {
        Log.i(TAG, "onSurfaceDestroyed");
        if (mFilterSdkManager != null) {
            mFilterSdkManager.destroy();
            mFilterSdkManager = null;
        }
    }

    @Override
    public int onDrawFrame(int texId, int texWidth, int texHeight) {
        int outTexId = texId;
        outTexId = mFilterSdkManager.onDrawFrame(texId, texWidth, texHeight);
        return outTexId;
    }

    // USaveFileListener
    @Override
    public void onVideoSaveProgress(float progress) {
        mActivity.runOnUiThread(() -> mProcessingDialog.setProgress((int) (100 * progress)));
    }

    @Override
    public void onVideoSaveSuccess(String s) {
    }

    @Override
    public void onVideoSaveCancel() {
        mProcessingDialog.dismiss();
    }

    @Override
    public void onVideoSaveFail(int errorCode) {
        Log.e("", "save edit failed errorCode:" + errorCode);
        mActivity.runOnUiThread(() -> {
            mProcessingDialog.dismiss();
            ToastUtil.INSTANCE.showToast(mActivity, getString(R.string.save_file_failed_tip) + errorCode);
        });
    }
}
