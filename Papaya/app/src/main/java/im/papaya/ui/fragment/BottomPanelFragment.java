package im.papaya.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;

import im.papaya.mvp.contract.FilterChangedListener;
import im.papaya.mvp.contract.OnCloseClickListener;
import im.papaya.mvp.model.entity.BeautyParamEnum;
import com.movieous.media.mvp.model.entity.UFilter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

import im.papaya.android.R;
import im.papaya.ui.base.BaseFragment;

/**
 * 贴纸选择页面
 */
public class BottomPanelFragment extends BaseFragment {
    protected static final String TAG = "BottomPanelFragment";

    protected Activity mActivity;
    protected LayoutInflater mInflater;
    protected OnCloseClickListener onCloseClickListener;

    public void setOnCloseClickListener(OnCloseClickListener listener) {
        onCloseClickListener = listener;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sticker_filter;
    }

    @Override
    public void lazyLoad() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = getActivity();
        mInflater = LayoutInflater.from(mActivity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void initView() {
    }
}
