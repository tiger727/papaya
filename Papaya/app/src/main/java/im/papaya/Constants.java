package im.papaya;

import android.os.Environment;

public class Constants {
    public static final String MOVIEOUS_SIGN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcHBpZCI6ImltLnBhcGF5YS5hbmRyb2lkIn0.Wbjly7_jbUcnHGIJPuEFb77ceVobDsvnRyV_8FJoLO4";

    public static final int DEFAULT_MAX_RECORD_DURATION = 15 * 1000;

    // 变速录制/编辑参数
    public static final double VIDEO_SPEED_SUPER_SLOW = 0.25;
    public static final double VIDEO_SPEED_SLOW = 0.5;
    public static final double VIDEO_SPEED_NORMAL = 1;
    public static final double VIDEO_SPEED_FAST = 2;
    public static final double VIDEO_SPEED_SUPER_FAST = 4;


    public static final String VIDEO_STORAGE_DIR = Environment.getExternalStorageDirectory().getPath() + "/im.papaya/shortvideo/";
    public static final String RECORD_FILE_PATH = VIDEO_STORAGE_DIR + "record.mp4";
    public static final int SPAN_COUNT = 6;
    public static final String EDIT_FILE_PATH = VIDEO_STORAGE_DIR + "edit.mp4";
    public static final String UPLOAD_FILE_PATH = VIDEO_STORAGE_DIR + "upload.mp4";
}
