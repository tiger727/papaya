package im.papaya.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.papaya.android.R;
import im.papaya.model.entity.VideoPlayListItem;
import im.papaya.ui.adapter.VideoItemAdapter;

public class VideoPlayActivity extends AppCompatActivity {

    private static final String TAG = "VideoPlayActivity";

    private Context mContext;
    private RecyclerView mVideoListRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private PagerSnapHelper mPagerSnapHelper;
    private static List<VideoPlayListItem> mVideoList;
    private boolean mIsPause;
    private int mPlayPosition;
    private View mPlayView;

    @BindView(R.id.iv_new_short_video)
    ImageView ivNewShortVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_video_play);

        ButterKnife.bind(this);
        mVideoList = getVideoList();
        initView();
    }

    private List<VideoPlayListItem> getVideoList() {
        List<VideoPlayListItem> videoItemBeanList = new ArrayList<>();

        videoItemBeanList.add(
                new VideoPlayListItem(
                        R.drawable.avatar1,
                        "https://api.amemv.com/aweme/v1/playwm/?video_id=v0200f7c0000bgfl6h9cgf374t645hfg&line=0&ratio=720p&media_type=4&vr_type=0&test_cdn=None&improve_bitrate=0",
                        "茹cut",
                        "#彭于晏 祝姐妹们的男朋友身材跟彭于晏一样棒\uD83D\uDD25",
                        "https://p9-dy.bytecdn.cn/large/14f9d00014cbcc1785d3a.jpeg",
                        720,
                        1280
                )
        );
        videoItemBeanList.add(
                new VideoPlayListItem(
                        R.drawable.avatar1,
                        "https://api.amemv.com/aweme/v1/playwm/?video_id=v0200f720000bgfo5g2j2bov50kp8nug&line=0&ratio=720p&media_type=4&vr_type=0&test_cdn=None&improve_bitrate=0",
                        "May",
                        "贫民窟女孩的艰辛",
                        "https://p3-dy.bytecdn.cn/large/14ee30005a250f311540b.jpeg",
                        720,
                        1280
                )
        );
        videoItemBeanList.add(
                new VideoPlayListItem(
                        R.drawable.avatar1,
                        "https://api.amemv.com/aweme/v1/playwm/?video_id=v0200fca0000bgff4okeae1ciqlplo6g&line=0&ratio=720p&media_type=4&vr_type=0&test_cdn=None&improve_bitrate=0",
                        "俺有正能量",
                        "#正能量 #励志 @抖音小助手 记住我一定会翻脸。",
                        "https://p3-dy.bytecdn.cn/large/14eb60005368f55e2320f.jpeg",
                        720,
                        1280
                )
        );
        videoItemBeanList.add(
                new VideoPlayListItem(
                        R.drawable.avatar1,
                        "https://api.amemv.com/aweme/v1/playwm/?video_id=v0200f720000bgfdmtt34q18rg9asfm0&line=0&ratio=720p&media_type=4&vr_type=0&test_cdn=None&improve_bitrate=0",
                        "请叫我田能能",
                        "南门九爷，超级喜欢",
                        "https://p9-dy.bytecdn.cn/large/14dbd00050704269301f9.jpeg",
                        720,
                        1280
                )
        );

        return videoItemBeanList;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mIsPause) {
            mIsPause = false;
            playVideo();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsPause = true;
        VideoItemAdapter.VideoViewHolder vh = (VideoItemAdapter.VideoViewHolder) mVideoListRecyclerView.getChildViewHolder(mPlayView);
        vh.videoView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VideoItemAdapter.VideoViewHolder vh = (VideoItemAdapter.VideoViewHolder) mVideoListRecyclerView.getChildViewHolder(mPlayView);
        vh.videoView.release();
    }

    private void initView() {
        mContext = VideoPlayActivity.this;
        mVideoListRecyclerView = findViewById(R.id.rv_video_detail);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        mVideoListRecyclerView.setLayoutManager(mLinearLayoutManager);
        mPagerSnapHelper = new PagerSnapHelper();
        mPagerSnapHelper.attachToRecyclerView(mVideoListRecyclerView);
        mVideoListRecyclerView.setAdapter(new VideoItemAdapter(mContext, mVideoList));
        mVideoListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE && mPagerSnapHelper.findSnapView(mLinearLayoutManager) != mPlayView) {
                    playVideo();
                }
            }
        });

        if (mVideoList != null && !mVideoList.isEmpty()) {
            mVideoListRecyclerView.post(() -> {
                mVideoListRecyclerView.scrollToPosition(mPlayPosition);
                mVideoListRecyclerView.post(() -> playVideo());
            });
        }
    }

    private void playVideo() {
        View snapView = mPagerSnapHelper.findSnapView(mLinearLayoutManager);
        if (snapView == null) {
            return;
        }
        final int position = mLinearLayoutManager.getPosition(snapView);
        if (position < 0) {
            return;
        }

        if (mPlayView != null) {
            final VideoItemAdapter.VideoViewHolder vh = (VideoItemAdapter.VideoViewHolder) mVideoListRecyclerView.getChildViewHolder(mPlayView);
            vh.videoView.setTag(vh.videoView.getVideoUri().toString());
            vh.videoView.stopPlayback();
        }

        mPlayView = snapView;
        mPlayPosition = position;
        final VideoItemAdapter.VideoViewHolder vh = (VideoItemAdapter.VideoViewHolder) mVideoListRecyclerView.getChildViewHolder(mPlayView);
        Log.i(TAG, "start play, url: " + vh.videoView.getVideoUri().toString());

        if (vh.videoView.getTag() == null || !vh.videoView.getTag().equals(vh.videoView.getVideoUri().toString())) {
            vh.videoView.start();
        } else {
            vh.videoView.restart();
        }
    }

    @OnClick(R.id.iv_new_short_video)
    public void onClickNewShortVideo() {
        startActivity(new Intent(VideoPlayActivity.this,	VideoRecordActivity.class));
    }

}
