package im.papaya.mvp.model.entity

class StickerFilterItem {
    var name: String? = null
    var filterPath: String? = null
    var thumbPath: String? = null
}
