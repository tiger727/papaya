package im.papaya.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.movieous.media.mvp.model.entity.UFilter;

import at.grabner.circleprogress.CircleProgressView;
import im.papaya.Constants;
import im.papaya.android.R;
import im.papaya.mvp.contract.IFaceBeautyParamChangedListener;
import im.papaya.mvp.contract.IFilterSelectedListener;
import im.papaya.mvp.contract.IMagicStickerSelectedListener;
import im.papaya.mvp.contract.OnCloseClickListener;
import im.papaya.mvp.model.entity.BeautyParamEnum;
import im.papaya.ui.fragment.BeautyFilterFragment;
import im.papaya.ui.fragment.StickerFilterFragment;
import im.papaya.utils.ThreadHelper;
import im.papaya.vendor.faceunity.FuSDKManager;
import im.papaya.vendor.faceunity.FuSDKUtil;
import video.movieous.engine.UVideoFrameListener;
import video.movieous.engine.UVideoSaveListener;
import video.movieous.engine.view.UTextureView;
import video.movieous.shortvideo.UMediaUtil;
import video.movieous.shortvideo.UVideoEditManager;


public class VideoEditorActivity extends AppCompatActivity
        implements UVideoFrameListener,
        OnCloseClickListener,
        IFaceBeautyParamChangedListener,
        IFilterSelectedListener,
        IMagicStickerSelectedListener {

    private static final String TAG = VideoEditorActivity.class.toString();
    public static final String PATH = "Path";
    public static final int REQUEST_CODE_CHOOSE = 1;
    private String videoPath;

    private UTextureView mPreview;
    //播放按钮
    private ImageView mPlayBtn;

    private UVideoEditManager mVideoEditManager;

    private FuSDKManager mFilterSdkManager;

    private ImageView iv_back;

    private Button bt_next;

    private Button btn_filters;
    private Button btn_stickers;
    private LinearLayout ll_bottom;
    private FrameLayout fragment_container;

    private StickerFilterFragment mStickerFilterFragment;
    private boolean mIsStickerFilterShowing = false;

    private BeautyFilterFragment mBeautyFilterFragment;
    private boolean mIsBeautyFilterShowing = false;

    /** 当前剪裁后的持续时间   微秒 **/
    private long mDurationTimeUs;

    //转码进度视图
    private FrameLayout mLoadContent;
    private CircleProgressView mLoadProgress;

    public static void start(Activity activity, String mp4Path) {
        Intent intent = new Intent(activity, VideoEditorActivity.class);
        intent.putExtra(PATH, mp4Path);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_editor);

        videoPath = getIntent().getStringExtra(PATH);
        if (TextUtils.isEmpty(videoPath)) {
            startVideoSelectActivity(this);
            return;
        }

        initVendorSDKManager();
        initView();

        startPlayer();
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        iv_back.setOnClickListener(view -> onBackPressed());

        bt_next = findViewById(R.id.bt_next);

        bt_next.setOnClickListener(view -> {
            if(mVideoEditManager.isPlaying()){
                pausePlayer();
                mPlayBtn.setVisibility(View.GONE);
            }
            mLoadContent.setVisibility(View.VISIBLE);


            mVideoEditManager.setVideoSaveListener(new UVideoSaveListener() {

                @Override
                public void onVideoSaveProgress(float progress) {
                    ThreadHelper.getInstance().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLoadContent.setVisibility(View.VISIBLE);
                            Log.d(TAG, String.format("%f", progress));
                            float percent = progress * 100;
                            mLoadProgress.setValue(percent);
//                            mLoadProgress.setAutoTextSize(true);
//                            mLoadProgress.setText(String.format("%d", (int)percent));
                        }
                    });
                }

                @Override
                public void onVideoSaveFail(int errorCode) {
                    mLoadContent.setVisibility(View.GONE);
                    mPlayBtn.setVisibility(View.VISIBLE);
                }

                @Override
                public void onVideoSaveSuccess(String path) {

                    PlaybackActivity.start(VideoEditorActivity.this, path);
                    finish();
                }
            });

            saveVideoFile();


        });
        mPreview = findViewById(R.id.video_view);
        mPlayBtn = findViewById(R.id.pause_playback);
        mVideoEditManager = new UVideoEditManager()
                .init(mPreview, videoPath);

        mVideoEditManager.setVideoFrameListener(this);

        mDurationTimeUs = UMediaUtil.getMetadata(videoPath).duration * 1000;

        mPlayBtn.setOnClickListener(v -> {
            if(mVideoEditManager == null)return;
            if (mVideoEditManager.isPlaying()) {
                pausePlayer();
            } else {
                startPlayer();
            }
        });

        mLoadContent = findViewById(R.id.lsq_editor_cut_load);
        mLoadProgress = findViewById(R.id.lsq_editor_cut_load_parogress);
        btn_filters = findViewById(R.id.btn_filters);
        btn_filters.setOnClickListener(onClickListener);
        btn_stickers = findViewById(R.id.btn_stickers);
        btn_stickers.setOnClickListener(onClickListener);

        ll_bottom = findViewById(R.id.ll_bottom);
        fragment_container = findViewById(R.id.fragment_container);

    }

    private void saveVideoFile() {
        mVideoEditManager.save(Constants.UPLOAD_FILE_PATH,  VideoEditorActivity.this);
    }

    public void startVideoSelectActivity(Activity activity) {
        activity.startActivity(new Intent(activity, VideoSelectActivity.class));
    }

    private void startPlayer() {
        mVideoEditManager.start();

        mPlayBtn.setVisibility(View.INVISIBLE);
    }

    private void pausePlayer() {
        mVideoEditManager.pause();
        mPlayBtn.setVisibility(View.VISIBLE);
    }

    private View.OnClickListener onClickListener = view -> {
        switch (view.getId())
        {
            case R.id.btn_filters:
                showFaceBeautyView();
                break;
            case R.id.btn_stickers:
                showStickerFilterLayout();
                break;
            default:
                break;
        }
    };

    private void showFaceBeautyView() {
        mIsBeautyFilterShowing = true;
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        if (mBeautyFilterFragment == null) {
            mBeautyFilterFragment = new BeautyFilterFragment();
            mBeautyFilterFragment.setuFilters(FuSDKUtil.getFilterList());
            mBeautyFilterFragment.setFilterSelectedListener(this);
            mBeautyFilterFragment.setFaceBeautyParamChangedListener(this);
            mBeautyFilterFragment.setOnCloseClickListener(this);
            ft.add(R.id.fragment_container, mBeautyFilterFragment);
        } else {
            ft.show(mBeautyFilterFragment);
        }
        ft.commit();

        showFragmentContainer();
        hideBottomLayout();
    }

    private void showStickerFilterLayout() {
        mIsStickerFilterShowing = true;
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        if (mStickerFilterFragment == null) {
            mStickerFilterFragment = new StickerFilterFragment();
            mStickerFilterFragment.setFilterItems(FuSDKUtil.getMagicFilterList());
            mStickerFilterFragment.setMagicStickerSelectedListener(this);
            mStickerFilterFragment.setOnCloseClickListener(this);
            ft.add(R.id.fragment_container, mStickerFilterFragment);
        } else {
            ft.show(mStickerFilterFragment);
        }
        ft.commit();
        showFragmentContainer();
        hideBottomLayout();
    }

    protected synchronized void initVendorSDKManager() {
        mFilterSdkManager = FuSDKManager.create(this);
    }

    @Override
    public void onSurfaceCreated() {
        mFilterSdkManager.onSurfaceCreated();
    }

    @Override
    public void onSurfaceChanged(int width, int height) {
        mFilterSdkManager.onSurfaceChanged(width, height);
    }

    @Override
    public void onSurfaceDestroyed() {
        Log.i(TAG, "onSurfaceDestroyed");
        if (mFilterSdkManager != null) {
            mFilterSdkManager.destroy();
            mFilterSdkManager = null;
        }
    }

    @Override
    public int onDrawFrame(int texId, int texWidth, int texHeight) {
        int outTexId = texId;
        outTexId = mFilterSdkManager.onDrawFrame(texId, texWidth, texHeight);
        return outTexId;
    }

    @Override
    public void onCloseClick() {
        hideFragmentContainer();
        showBottomLayout();
    }

    private void showBottomLayout() {
        ll_bottom.setVisibility(View.VISIBLE);
    }

    private void hideBottomLayout() {
        ll_bottom.setVisibility(View.GONE);
    }

    private void showFragmentContainer(){
        fragment_container.setVisibility(View.VISIBLE);
    }

    private void hideFragmentContainer() {

        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        if (mIsStickerFilterShowing) {
            ft.hide(mStickerFilterFragment);
            mIsStickerFilterShowing = false;
        }
        else if(mIsBeautyFilterShowing){
            ft.hide(mBeautyFilterFragment);
            mIsBeautyFilterShowing = false;
        }
        ft.commit();

        fragment_container.setVisibility(View.GONE);
    }

    @Override
    public void onBeautyParamChanged(BeautyParamEnum beautyType, float value) {
        mFilterSdkManager.onBeautyParamChanged(beautyType, value);
    }

    @Override
    public void onMagicStickerSelected(UFilter filter) {
        mFilterSdkManager.onMagicStickerSelected(filter);
    }

    @Override
    public void onFilterSelected(UFilter filter) {
        mFilterSdkManager.onFilterSelected(filter);
    }
}
