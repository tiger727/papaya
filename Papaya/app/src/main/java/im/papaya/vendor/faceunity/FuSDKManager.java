package im.papaya.vendor.faceunity;

import android.content.Context;
import android.util.Log;

import com.faceunity.FURenderer;
import com.faceunity.entity.Effect;
import com.faceunity.entity.Filter;
import com.movieous.media.mvp.model.entity.UFilter;

import im.papaya.mvp.model.entity.BeautyParamEnum;

public class FuSDKManager {
    private static final String TAG = "FuSDKManager";

    private FURenderer mFURenderer;

    private FuSDKManager() {
    }

    public static FuSDKManager create(Context context){
        FuSDKManager fuSDKManager = new FuSDKManager();

        fuSDKManager.initRenderer(context);

        return fuSDKManager;
    }

    private void initRenderer(Context context) {
        //初始化FU相关 authpack 为证书文件
        mFURenderer = new FURenderer
                .Builder(context)
                .build();
    }

    public void onSurfaceCreated() {
        mFURenderer.onSurfaceCreated();
    }

    public void onSurfaceChanged(int width, int height) {
        Log.i(TAG, "onSurfaceChanged");
    }

    public void destroy() {
        mFURenderer.onSurfaceDestroyed();
    }

    public int onDrawFrame(int texId, int texWidth, int texHeight) {
        return mFURenderer.onDrawFrame(texId, texWidth, texHeight);
    }

    public void onBeautyParamChanged(BeautyParamEnum beautyType ,float value) {
        switch (beautyType) {
            case FACE_BLUR:
                mFURenderer.onBlurLevelSelected(value);
                break;
            case EYE_ENLARGE:
                mFURenderer.onEyeEnlargeSelected(value);
                break;
            case CHEEK_THINNING:
                mFURenderer.onCheekThinningSelected(value);
                break;
        }
    }

    public void onMagicStickerSelected(UFilter filter) {
        Log.i(TAG, "changeFilter: filter = " + filter.getName());
        Effect effect = new Effect(filter.getName(), filter.getResId(), filter.getPath(), filter.getMaxFace(), filter.getType(), filter.getDescription());
        mFURenderer.onEffectSelected(effect);
    }

    public void onFilterSelected(UFilter filterItem) {
        Filter filter = new Filter(filterItem.getName(), filterItem.getResId(), filterItem.getDescription());
        mFURenderer.onFilterNameSelected(filter.filterName());

    }
}
