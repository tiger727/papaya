package im.papaya.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.movieous.media.mvp.model.entity.UFilter;

import java.util.List;

import im.papaya.android.R;
import im.papaya.mvp.contract.IFilterSelectedListener;
import im.papaya.mvp.model.entity.FilterVendor;
import im.papaya.mvp.model.entity.MediaParam;
import im.papaya.utils.SharePrefUtils;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.HomeRecyclerHolder> {
    private int mFilterPositionSelect = 0;
    private int mFilterTypeSelect = UFilter.Companion.getFILTER_TYPE_BEAUTY_FILTER();
    private int filterType = UFilter.Companion.getFILTER_TYPE_FILTER();

    private IFilterSelectedListener filterSelectedListener;
    private List<UFilter> uFilters;
    public FilterVendor mFilterVendor;

    public FilterAdapter(Context context, IFilterSelectedListener listener, List<UFilter> uFilters) {
        filterSelectedListener = listener;
        this.uFilters = uFilters;
        MediaParam param = SharePrefUtils.getParam(context);
        mFilterVendor = param.vendor;

    }

    @Override
    public FilterAdapter.HomeRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FilterAdapter.HomeRecyclerHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_view, parent, false));
    }

    @Override
    public void onBindViewHolder(FilterAdapter.HomeRecyclerHolder holder, final int position) {
        final List<UFilter> filters = getItems(filterType);
        UFilter filter = filters.get(position);
        if (mFilterVendor == FilterVendor.FACEUNITY) {
            holder.filterImg.setImageResource(filter.getResId());
            holder.filterName.setText(filter.getDescription());
        } else {
            holder.filterImg.setImageBitmap(filter.getIcon());
            holder.filterName.setText(filter.getName());
        }
        if (mFilterPositionSelect == position && filterType == mFilterTypeSelect) {
            holder.filterImg.setBackgroundResource(R.drawable.control_filter_select);
        } else {
            holder.filterImg.setBackgroundResource(0);
        }
        holder.itemView.setOnClickListener(v -> {
            mFilterPositionSelect = position;
            mFilterTypeSelect = filterType;
            //setFilterProgress();
            notifyDataSetChanged();
            if(filterSelectedListener != null){
                filterSelectedListener.onFilterSelected(filters.get(mFilterPositionSelect));
            }
        });
    }

    @Override
    public int getItemCount() {
        return getItems(filterType).size();
    }

    public void setFilterType(int filterType) {
        this.filterType = filterType;
        notifyDataSetChanged();
    }

    public List<UFilter> getItems(int type) {
        return uFilters;
    }

    public class HomeRecyclerHolder extends RecyclerView.ViewHolder {

        ImageView filterImg;
        TextView filterName;

        public HomeRecyclerHolder(View itemView) {
            super(itemView);
            filterImg = itemView.findViewById(R.id.icon);
            filterName = itemView.findViewById(R.id.name);
            filterName.setVisibility(View.VISIBLE);
        }
    }
}
