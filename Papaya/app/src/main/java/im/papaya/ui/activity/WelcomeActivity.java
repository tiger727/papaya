package im.papaya.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.faceunity.FURenderer;

import java.util.Arrays;
import java.util.List;

import im.papaya.android.R;
import im.papaya.utils.AppUtils;
import pub.devrel.easypermissions.EasyPermissions;

public class WelcomeActivity extends AppCompatActivity {

    private ImageView iv_logo;
    private TextView tv_app_name;
    private TextView tv_version;
    private AlphaAnimation alphaAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        initView();
        initData();
    }

    private void initData() {

        String versionCode = Integer.toString(AppUtils.getVerName(this));
        tv_version.setText(versionCode);

        FURenderer.initFURenderer(this);
    }

    private void initView() {
        iv_logo = findViewById(R.id.iv_logo);
        tv_app_name = findViewById(R.id.tv_app_name);
        tv_version = findViewById(R.id.tv_version);

        alphaAnimation = new AlphaAnimation(0.3f, 1.0f);
        alphaAnimation.setDuration(2000);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startMainActivity();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        checkPermission();
    }

    private void startMainActivity() {
        startActivity(new Intent(WelcomeActivity.this,	VideoPlayActivity.class));
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        List<String> perms = Arrays.asList(permissions);

        if (perms.size() > 0 && this.alphaAnimation != null
                && perms.contains("android.permission.READ_PHONE_STATE")
                && perms.contains("android.permission.WRITE_EXTERNAL_STORAGE")) {
            iv_logo.startAnimation(this.alphaAnimation);
        }
    }

    private final void checkPermission() {
        String[] perms = new String[]{
                "android.permission.READ_PHONE_STATE",
                "android.permission.WRITE_EXTERNAL_STORAGE",
                "android.permission.RECORD_AUDIO",
                "android.permission.CAMERA"
        };
        EasyPermissions.requestPermissions(this, this.getString(R.string.request_check_notice), 0, (String[]) Arrays.copyOf(perms, perms.length));
    }
}
