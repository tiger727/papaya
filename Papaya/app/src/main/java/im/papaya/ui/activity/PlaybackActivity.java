package im.papaya.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import at.grabner.circleprogress.CircleProgressView;
import iknow.android.utils.thread.UiThreadExecutor;
import im.papaya.Constants;
import im.papaya.android.R;
import im.papaya.utils.AppUtils;
import im.papaya.utils.DisplayManager;
import im.papaya.utils.GetPathFromUri;
import im.papaya.utils.StringUtils;
import im.papaya.utils.ThreadHelper;
import im.papaya.utils.ToastUtil;
import im.papaya.view.editor.EditorCutView;
import im.papaya.view.editor.playview.TuSdkRangeSelectionBar;
import video.movieous.engine.UMediaTrimTime;
import video.movieous.engine.UVideoFrameListener;
import video.movieous.engine.UVideoSaveListener;
import video.movieous.engine.base.callback.SingleCallback;
import video.movieous.engine.view.UTextureView;
import video.movieous.shortvideo.UMediaUtil;
import video.movieous.shortvideo.UVideoEditManager;
import video.movieous.shortvideo.UVideoPlayListener;


public class PlaybackActivity extends AppCompatActivity implements UVideoFrameListener {
    private static final String TAG = "PlaybackActivity";
    private static final String MP4_PATH = "MP4_PATH";

    private UTextureView mPreview;
    //底部裁剪的控件
    private EditorCutView mEditorCutView;
    //播放按钮
    private ImageView mPlayBtn;

    private UVideoEditManager mVideoEditManager;

    private ImageView iv_back;

    private Button bt_next;

    private UMediaTrimTime mTrimTime;

    //转码进度视图
    private FrameLayout mLoadContent;
    private CircleProgressView mLoadProgress;

    /** 当前剪裁后的持续时间   微秒 **/
    private long mDurationTimeUs;
    /** 左边控件选择的时间     微秒 **/
    private long mLeftTimeRangUs;
    /** 右边控件选择的时间     微秒**/
    private long mRightTimeRangUs;
    /** 最小裁切时间 */
    private long mMinCutTimeUs = 3 * 1000000;

    /** 是否已经设置总时间 **/
    private boolean isSetDuration = false;
    private String videoPath = "";

    public static void start(Activity activity, String mp4Path) {

        Intent intent = new Intent(activity, PlaybackActivity.class);
        intent.putExtra(MP4_PATH, mp4Path);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        String videoPath = getIntent().getStringExtra(MP4_PATH);
        if (StringUtils.INSTANCE.isEmpty(videoPath)) {
            startActivityForResult(Intent.createChooser(AppUtils.getMediaIntent(true), getString(R.string.select_media_file_tip)), VideoEditorActivity.REQUEST_CODE_CHOOSE);
            return;
        }
        setContentView(R.layout.activity_playback);
        this.videoPath = videoPath;
        initView();
        loadThumbnails();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.hold, R.anim.zoom_exit);
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        iv_back.setOnClickListener(view -> onBackPressed());

        bt_next = findViewById(R.id.bt_next);

        bt_next.setOnClickListener(view -> {
            if(mVideoEditManager.isPlaying()){
                pausePlayer();
                mPlayBtn.setVisibility(View.GONE);
            }
            mEditorCutView.setEnable(false);
            mLoadContent.setVisibility(View.VISIBLE);


            mVideoEditManager.setVideoSaveListener(new UVideoSaveListener() {

                @Override
                public void onVideoSaveProgress(float progress) {
                    ThreadHelper.getInstance().runOnUiThread(() -> {
                        mLoadContent.setVisibility(View.VISIBLE);
                        Log.d(TAG, String.format("%f", progress));
                        float percent = progress * 100;
                        mLoadProgress.setValue(percent);
//                        mLoadProgress.setAutoTextSize(true);
//                        mLoadProgress.setText(String.format("%d", (int)percent));
                    });
                }

                @Override
                public void onVideoSaveFail(int errorCode) {
                    mLoadContent.setVisibility(View.GONE);
                    mPlayBtn.setVisibility(View.VISIBLE);
                }

                @Override
                public void onVideoSaveSuccess(String path) {
                    VideoEditorActivity.start(PlaybackActivity.this, path);
                    finish();
                }
            });

            mVideoEditManager.save(Constants.EDIT_FILE_PATH);
        });
        mPreview = findViewById(R.id.video_view);
        mEditorCutView = new EditorCutView(this);
        mPlayBtn = findViewById(R.id.pause_playback);
        mVideoEditManager = new UVideoEditManager()
                .init(mPreview, videoPath);
        mDurationTimeUs = UMediaUtil.getMetadata(videoPath).duration * 1000;
        float duration = mDurationTimeUs / 1000000.0f;
        mRightTimeRangUs = mDurationTimeUs;
        mEditorCutView.setRangTime(duration);
        mEditorCutView.setTotalTime(mDurationTimeUs);

        mTrimTime = new UMediaTrimTime(0, (int)mRightTimeRangUs/1000);

        mVideoEditManager.setTrimTime(mTrimTime);

        mEditorCutView.setOnPlayPointerChangeListener(percent -> {

            if (mVideoEditManager.isPlaying()) {
                pausePlayer();
            }

            mVideoEditManager.seekTo(getSeekTimeMs(percent));
            mVideoEditManager.start();
        });

        mEditorCutView.getLineView().setExceedCriticalValueListener(new TuSdkRangeSelectionBar.OnExceedCriticalValueListener() {
            @Override
            public void onMaxValueExceed() {

            }

            @Override
            public void onMinValueExceed() {
                Integer minTime = (int) (mMinCutTimeUs / 1000000);
                @SuppressLint("StringFormatMatches") String tips = String.format(getString(R.string.lsq_min_time_effect_tips), minTime);
                ToastUtil.INSTANCE.showToast(PlaybackActivity.this, tips);
            }
        });

        mEditorCutView.setOnSelectCeoverTimeListener(new TuSdkRangeSelectionBar.OnSelectRangeChangedListener() {
            @Override
            public void onSelectRangeChanged(float leftPercent, float rightPercent, int type) {
                if(type == 0 ){
                    mLeftTimeRangUs = (long) (leftPercent * mDurationTimeUs);
                    float selectTime = (mRightTimeRangUs - mLeftTimeRangUs) / 1000000.0f;
                    if(selectTime < 3.0)selectTime = 3.0f;
                    mEditorCutView.setRangTime(selectTime);
                    if(mVideoEditManager.isPlaying()){
                        pausePlayer();
                    }
                    mEditorCutView.setVideoPlayPercent(leftPercent);

                    mVideoEditManager.seekTo(getSeekTimeMs(leftPercent));

                    mTrimTime.startTimeMs = (int)mLeftTimeRangUs/1000;
                    mVideoEditManager.setTrimTime(mTrimTime);

                }else if(type == 1){
                    mRightTimeRangUs = (long) (rightPercent * mDurationTimeUs);
                    float selectTime = (mRightTimeRangUs - mLeftTimeRangUs) / 1000000.0f;
                    if(selectTime < 3.0)selectTime = 3.0f;
                    mEditorCutView.setRangTime(selectTime);
                    if(mVideoEditManager.isPlaying()){
                        pausePlayer();
                    }
                    mEditorCutView.setVideoPlayPercent(rightPercent);
                    mVideoEditManager.seekTo(getSeekTimeMs(rightPercent));

                    mTrimTime.endTimeMs = (int)mRightTimeRangUs/1000;
                    mVideoEditManager.setTrimTime(mTrimTime);
                }
            }
        });

        mPlayBtn.setOnClickListener(v -> {
            if(mVideoEditManager == null)return;
            if (mVideoEditManager.isPlaying()) {
                pausePlayer();
            } else {
                startPlayer();
            }
        });

        mVideoEditManager.setVideoPlayerListener(new UVideoPlayListener() {
            @Override
            public void onPositionChanged(int position) {

                float percent = (float)position * 1000/mDurationTimeUs;

                Log.i(TAG, String.format("current position: %d", position));
                mEditorCutView.setVideoPlayPercent(percent);
            }
        }, 20);

        mLoadContent = findViewById(R.id.lsq_editor_cut_load);
        mLoadProgress = findViewById(R.id.lsq_editor_cut_load_parogress);

    }

    private void startPlayer() {
        mVideoEditManager.start();

        mPlayBtn.setVisibility(View.INVISIBLE);
//        setPlayButtonState(R.drawable.btn_pause);
    }

    private void pausePlayer() {
        mVideoEditManager.pause();
        mPlayBtn.setVisibility(View.VISIBLE);
//        setPlayButtonState(R.drawable.btn_play);
    }

    private void setPlayButtonState(int resId) {
        if (mPlayBtn == null) return;
        this.runOnUiThread(() -> mPlayBtn.setImageResource(resId));
    }
    private int getSeekTimeMs(float percent){
        int seekTime = (int) (mDurationTimeUs*percent) / 1000;

        return seekTime;
    }

    private void loadThumbnails() {
        UMediaUtil.getVideoThumb(Uri.parse(videoPath), 20, 0, getTimeInMs(mDurationTimeUs), DisplayManager.INSTANCE.dip2px(40), DisplayManager.INSTANCE.dip2px(40),
                (SingleCallback<Bitmap, Long>) (bitmap, pts) -> {
                    if (bitmap != null) {
                        UiThreadExecutor.runTask("", () -> {
                            mEditorCutView.addBitmap(bitmap);
                            if(!isSetDuration) {
                                float duration = mDurationTimeUs / 1000000.0f;
                                mEditorCutView.setRangTime(duration);
                                mEditorCutView.setTotalTime(mDurationTimeUs);
                                if(duration >0)
                                    isSetDuration = true;
                            }
                            mEditorCutView.setMinCutTimeUs(mMinCutTimeUs/(float)mDurationTimeUs);
                        }, 0L);
                    }
                });
    }

    private long getTimeInMs(long timeInUs){
        return timeInUs/1000;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mVideoEditManager != null) {
            mVideoEditManager.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mVideoEditManager != null) {
            mVideoEditManager.start();
        }

        if(mEditorCutView != null)
            mEditorCutView.setEnable(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVideoEditManager != null) {
            mVideoEditManager.release();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == VideoEditorActivity.REQUEST_CODE_CHOOSE) {
                String selectedFilepath = GetPathFromUri.INSTANCE.getPath(this, data.getData());
                if (selectedFilepath != null && !"".equals(selectedFilepath)) {
                    start(PlaybackActivity.this, selectedFilepath);
                    finish();
                }
            }
        } else {
            finish();
        }
    }
}