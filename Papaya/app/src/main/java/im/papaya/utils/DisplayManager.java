package im.papaya.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import im.papaya.android.R;

public class DisplayManager {
    private static DisplayMetrics displayMetrics;
    private static Integer screenWidth;
    private static Integer screenHeight;
    private static Integer screenDpi;
    private static final int STANDARD_WIDTH = 1080;
    private static final int STANDARD_HEIGHT = 1920;
    public static final DisplayManager INSTANCE;

    private DisplayManager() {
    }

    static {
        INSTANCE = new DisplayManager();
    }

    public final void init(@NotNull Context context) {
        Resources resources = context.getResources();
        displayMetrics = resources.getDisplayMetrics();
        screenWidth = displayMetrics != null ? displayMetrics.widthPixels : null;
        screenHeight = displayMetrics != null ? displayMetrics.heightPixels : null;
        screenDpi = displayMetrics != null ? displayMetrics.densityDpi : null;
    }

    @Nullable
    public final Integer getScreenWidth() {
        return screenWidth;
    }

    @Nullable
    public final Integer getScreenHeight() {
        return screenHeight;
    }

    @Nullable
    public final Integer getPaintSize(int size) {
        return this.getRealHeight(size);
    }

    @Nullable
    public final Integer getRealWidth(int px) {
        return this.getRealWidth(px, (float)1080);
    }

    @Nullable
    public final Integer getRealWidth(int px, float parentWidth) {
        float pixelUnit = (float)px / parentWidth;
        Integer width = this.getScreenWidth();

        return (int)(pixelUnit * (float)width);
    }

    @Nullable
    public final Integer getRealHeight(int px) {
        return this.getRealHeight(px, (float)1920);
    }

    @Nullable
    public final Integer getRealHeight(int px, float parentHeight) {
        float pixelUnit = (float)px / parentHeight;
        Integer height = this.getScreenHeight();

        return (int)(pixelUnit * (float)height);
    }

    @Nullable
    public final Integer dip2px(float dipValue) {
        Float scale = displayMetrics != null ? displayMetrics.density : null;

        return (int)(dipValue * scale + 0.5F);
    }

    public final int getItemSize(Context context, int spanCount) {

        int mImageResize;

        int availableWidth = screenWidth - context.getResources().getDimensionPixelSize(
                R.dimen.media_grid_spacing) * (spanCount - 1);
        mImageResize = availableWidth / spanCount;
//        mImageResize = (int) (mImageResize * 0.5f);

        return mImageResize;
    }
}
