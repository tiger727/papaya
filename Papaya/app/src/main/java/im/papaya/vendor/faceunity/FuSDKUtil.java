package im.papaya.vendor.faceunity;

import com.faceunity.entity.Effect;
import com.faceunity.entity.Filter;
import com.movieous.media.mvp.model.entity.UFilter;

import java.util.ArrayList;
import java.util.List;

import im.papaya.mvp.model.entity.FilterVendor;

public class FuSDKUtil {

    public static List<UFilter> getFilterList() {
        List<UFilter> filterItemList = new ArrayList<>();
        ArrayList<Filter> beautyFilterList = im.papaya.vendor.faceunity.entity.FilterEnum.getFiltersByFilterType();
        for (Filter filter : beautyFilterList) {
            UFilter item = new UFilter(filter.filterName(), filter.description());
            item.setResId(filter.resId());
            filterItemList.add(item);
        }
        return filterItemList;
    }

    public static ArrayList<UFilter> getMagicFilterList() {

        ArrayList<UFilter> items = new ArrayList<>();

        int[] types = new int []{Effect.EFFECT_TYPE_NONE, Effect.EFFECT_TYPE_NORMAL, Effect.EFFECT_TYPE_AR};

        for (int type : types) {
            ArrayList<Effect> mEffects = im.papaya.vendor.faceunity.entity.EffectEnum.getEffectsByEffectType(type);
            for (Effect effect : mEffects) {
                UFilter item = new UFilter(effect.bundleName(), effect.path());
                item.setVendor(FilterVendor.FACEUNITY);
                item.setResId(effect.resId());
                item.setMaxFace(effect.maxFace());
                item.setType(effect.effectType());
                item.setDescription(effect.description());
                items.add(item);
            }
        }

        return items;
    }
}
