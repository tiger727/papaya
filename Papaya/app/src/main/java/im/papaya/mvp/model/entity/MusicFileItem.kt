package im.papaya.mvp.model.entity

class MusicFileItem {
    var title: String? = null // 歌曲名称
    var path: String? = null  // 路径
    var artist: String? = null // 歌手
}
