package com.faceunity.greendao;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.faceunity.entity.AvatarModel;
import com.faceunity.entity.LivePhoto;

import com.faceunity.greendao.AvatarModelDao;
import com.faceunity.greendao.LivePhotoDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig avatarModelDaoConfig;
    private final DaoConfig livePhotoDaoConfig;

    private final AvatarModelDao avatarModelDao;
    private final LivePhotoDao livePhotoDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        avatarModelDaoConfig = daoConfigMap.get(AvatarModelDao.class).clone();
        avatarModelDaoConfig.initIdentityScope(type);

        livePhotoDaoConfig = daoConfigMap.get(LivePhotoDao.class).clone();
        livePhotoDaoConfig.initIdentityScope(type);

        avatarModelDao = new AvatarModelDao(avatarModelDaoConfig, this);
        livePhotoDao = new LivePhotoDao(livePhotoDaoConfig, this);

        registerDao(AvatarModel.class, avatarModelDao);
        registerDao(LivePhoto.class, livePhotoDao);
    }
    
    public void clear() {
        avatarModelDaoConfig.clearIdentityScope();
        livePhotoDaoConfig.clearIdentityScope();
    }

    public AvatarModelDao getAvatarModelDao() {
        return avatarModelDao;
    }

    public LivePhotoDao getLivePhotoDao() {
        return livePhotoDao;
    }

}
