package im.papaya.mvp.contract;

public interface OnItemClickListener<T> {

    void onItemClicked(T item);
}
