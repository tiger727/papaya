package im.papaya.mvp.contract;

public interface OnCloseClickListener {

    void onCloseClick();
}
