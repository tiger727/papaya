package im.papaya.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import java.util.List;

import im.papaya.android.R;
import im.papaya.mvp.contract.OnItemClickListener;
import im.papaya.mvp.model.entity.VideoFileItem;
import im.papaya.ui.adapter.VideoSelectionListAdapter;
import im.papaya.utils.DisplayManager;
import im.papaya.utils.Utils;

public class VideoSelectActivity extends AppCompatActivity implements OnItemClickListener<VideoFileItem> {

    private RecyclerView recyclerView;
    private ImageView iv_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_select);

        iv_back = findViewById(R.id.iv_back);
        iv_back.setOnClickListener(view -> onBackPressed());
        recyclerView = findViewById(R.id.rv_video_list);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,3);

        int itemSize = DisplayManager.INSTANCE.getItemSize(this, 3);
        recyclerView.setLayoutManager(layoutManager);
        List<VideoFileItem> items = Utils.getVideoList(this);
        VideoSelectionListAdapter adapter = new VideoSelectionListAdapter(getApplicationContext(), items, itemSize, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClicked(VideoFileItem item) {
        PlaybackActivity.start(this, item.getPath());
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.hold, R.anim.zoom_exit);
    }
}
