package im.papaya;

import android.app.Application;
import android.content.Context;


import com.faceunity.greendao.GreenDaoUtils;
import com.faceunity.utils.FileUtils;

import im.papaya.utils.DisplayManager;
import im.papaya.utils.ThreadHelper;
import video.movieous.droid.player.MovieousPlayerEnv;
import video.movieous.engine.base.utils.ULog;
import video.movieous.shortvideo.UShortVideoEnv;

public final class PapayaApplication extends Application {

    public Context context;
    private static long cacheMaxBytes = 100L * 1024L * 1024L;
    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        DisplayManager.INSTANCE.init(this);
        initShortVideoEnv();
        initMovieousPlayerEnv();
        initFuSdkEnv();
    }

    private void initFuSdkEnv() {
        ThreadHelper.getInstance().execute(() -> {
            // 拷贝 assets 资源
            FileUtils.copyAssetsLivePhoto(context);
            FileUtils.copyAssetsTemplate(context);
            // 初始化数据库，一定在拷贝文件之后
            GreenDaoUtils.initGreenDao(context);
        });
    }

    private void initMovieousPlayerEnv() {
        MovieousPlayerEnv.init(context);
        // 开启本地缓存，可以离线播放, 需要 okhttp 支持
        MovieousPlayerEnv.setCacheInfo(this.getCacheDir(), null, cacheMaxBytes , "MovieousPlayer20", true);

    }

    private void initShortVideoEnv() {
        UShortVideoEnv.setLogLevel(ULog.I);
        UShortVideoEnv.init(context, Constants.MOVIEOUS_SIGN);
    }
}
