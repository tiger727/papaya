package im.papaya.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import im.papaya.android.R;
import im.papaya.ui.fragment.VideoRecordFragment;

public class VideoRecordActivity extends AppCompatActivity {

    private static final String TAG = VideoRecordActivity.class.toString();
    private VideoRecordFragment mVideoRecordFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_base);
        if (null == savedInstanceState) {
            mVideoRecordFragment = new VideoRecordFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, mVideoRecordFragment)
                    .commit();
        }
    }
}
