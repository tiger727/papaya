package im.papaya.mvp.contract;

import com.movieous.media.mvp.model.entity.UFilter;

public interface IMagicStickerSelectedListener {
    void onMagicStickerSelected(UFilter uFilter);
}
